<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('form');
});

//Route::post('/saludo', function () {
//    return view('greet');
//});

Route::post('/saludo', function () {
 //   return view('greet'.compact('edad'));
   $name = $_POST['name']; 
   return view('greet')->with('name',$name);
});
