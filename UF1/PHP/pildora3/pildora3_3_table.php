<table>

<?php
//Variables declaration
$player_names = $_POST["name_player"];
$player_goals = $_POST["player_goals"];
?>

<tr>
<th>Jugador</th>

<?php
// Header
for ($i = 0; $i < count($player_goals[0]); $i++ ){
	echo '<th>Partit ' . ($i + 1) . '</th>';
};
?>

</tr>

<?php
// Table data
for ($i = 0; $i < count($player_names); $i++) {
	echo '<tr>';
	echo '<td>' . $player_names[$i] . '</td>';
	for ($j = 0; $j < count($player_goals[$i]); $j++) {
		echo '<td>' . $player_goals[$i][$j] . '</td>';
	};
	echo '</tr>';
};
?>

<table>
