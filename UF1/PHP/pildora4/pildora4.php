<?php
// FUCNTIONS
// 1.

// Variable declartion past from a form
$number_to_sum = $_POST['num'];
/*
* Sum five numbers past as parameters and prints the result
*/
function sumFiveNum($num1, $num2, $num3, $num4, $num5) {
	$result = ($num1 + $num2 + $num3 + $num4 + $num5);
	echo $result;
}


// 2.
/**
* Sum five numbers past as parameters and return the result
*/
function getSumFiveNum($num1, $num2, $num3, $num4, $num5) {
	$result = $sum1 + $num2 + $num3 + $num4 + $num5;
	return $result;
}
$temp = getSumFiveNum(2, 5, 1, 8, 10);


// 3. 
// Variables declaration past from a form
$radius = $_POST['radius'];
$heigth = $_POST['heigth'];
/*
* Calculate the volume form a cylinder.
*/
function getVolume($radiusBase, $heigth) {
	$numPi = 3.1416;
	$volume = $numPi * $radiusBase * $radiusBase * $heigth;
	return $volume;
}
// Use of the function get volume.
$volume_cilinder = getVolume($radius, $heigth);


// 4.
// Variable declaration past from a form
$purchase_price = $_POST['price'];
/**
* Calculate a discount based in the price
*/ 
function calculateDiscount($price) {
	// If the price is bigger than 500 a 15% discount is applied
	if ($price >= 500) {
		$price = $price - ($price * 0.15); 
	// If the price is between 100 and 500 a 10% discount is applied
	} else if ($price >= 100 && $price < 500){
		$price = $price - ($price * 0.10);
	}
return $price;
}
// Use the function calculateDiscount
$price_with_discount = calculateDiscount($purchase_price);


// 5.
// Variable declaration past from a form
$compare = $_POST['compare'];
/**
* Compare two numbers an return 1, 0 or -1 if the first, none or the second is bigger
*/ 
function relacion($a, $b) {
	$answer = 0;
	if ($a > $b) {
		$answer = 1;	
	} else if ($a < $b) {
		$answer = -1;	
	}
	return $answer;
}
// Using relacion function
$comparation_result = relacion($compare[0], $compare[1]);


// 6.
// Variable declaration get from a form
$middle = $_POST['middle'];
/**
* Calculate the middle point between two numbers.
*/
function intermedio($a, $b) { 
	$middle_point = (($b + $a) / 2);
	return $middle_point;
}
// Using intermedio function
$mid_point = intermedio($middle[0], $middle[1]);


// 7.
// Variable declaration. Array to separate
$numeros_a_separar = [-12, 84, 13, 20, -33, 101, 9];
/**
* Separate, into an multidimensional array, an array of numbers in even or odd
*/
function separar($list) {
	// Sort the list
	sort($list);
	// Declaration of the multidimensional array
	$separated_list = array(
		'even' => array(),
		'odd' => array(),
	);
	// Traverse the given list
	for ($i = 0; $i < count($list); $i++) {
		if ($list[$i] % 2 == 0) {
			$separated_list['even'][] = $list[$i];
		} else {
			$separated_list['odd'][] = $list[$i];
		}
	}		
	return $separated_list;
}
// Using separar function
$separated = separar($numeros_a_separar);

// POO
// 8.
// Variables getting two employes from a form
$employee1 = $_POST['employee1'];
$employee2 = $_POST['employee2'];

/**
Class that generate an Employee
*/
class Employee {
	// Properties
	private $name; 		// The employee's name
	private $salary;	// The employee's salaray
	
	/**
	* Set the employee's properties.
	*/
	public function inicializar($name, $sal) {
		$this->name = $name;
		$this->salary = $sal;
	}	

	/**
	* Print employee's name and wheter they has to pay taxes or not
	*/
	public function impuestos() {
		echo 'Nombre empleado: ' . $this->name . "\t";
		if ($this->salary > 3000) {
			echo "SÍ té que pagar impostos";
		} else {
			echo "NO té que pagar impostos";
		}
	}
}

// Initialize two employes
$emp1 = new Employee();
$emp2 = new Employee();
// Set employees properties
$emp1->inicializar($employee1[0], $employee1[1]);
$emp2->inicializar($employee2[0], $employee2[1]);


// 9.
/**
Class that generates a list of links
*/
class LinkList {
	// linkList list
	private $linkList = array();

	
	// Charge a link to the list
	// To better perfomance the array should include other keys such as: name, link, ...
	public function cargarOpcion($link) {
		$this->linkList[] = $link;
	}

	// Show the list
	public function mostrar() {
		echo '<ul>';
		for ($i = 0; $i < count($this->linkList); $i++) {
			echo '<li style="display: inline-block;"> <a href="' . $this->linkList[$i]  .'">Link ' . ($i + 1) . '</a></li>';
		}
		echo '</ul>';
	}
}

// Vreate a new object linkList
$list1 = new LinkList();

// Load options
$list1->cargarOpcion('http://wiki.dantriano.com/daw/m7/index');
$list1->cargarOpcion('https://repl.it/');
$list1->cargarOpcion('http://www.escoladeltreball.org/ca/');


// 10.
/**
Class that generates a page header
*/
class PageHeader {
	// Properties
	private $title;				// The page header title
	private $position;			// The page header position (right,center,left) ?
	private $background_color;	// The header background color
	private $font_color;		// The page header font color

	// Set the initial properties
	public function initialice($title, $position, $back_color, $font_color) {
		$this->title = $title;
		$this->position = $position;
		$this->background_color = $back_color;
		$this->font_color = $font_color;
	}

	// Show the header into a page
	public function show() {
		echo '<h1 ';
		echo 'style="text-align: ' . $this->position . '; ';
		echo 'background-color: ' . $this->background_color . '; ';
		echo 'color: ' . $this->font_color . '; ';
		echo '">' . $this->title . '</h1>';
	}
	
}
// Get the values past by the form
$title_options = $_POST['title'];
// Generates a new PageHeader object
$page_header = new PageHeader();
// Set the values
$page_header->initialice($title_options[0],$title_options[1],$title_options[2],$title_options[3]);


// 11.
/**
Class that generates a page header
*/
class PageHeader2 {
	// Properties
	private $title;				// The page header title
	private $position;			// The page header position (right,center,left) ?
	private $background_color;	// The header background color
	private $font_color;		// The page header font color

	// Constructor
	public function __construct($title, $position, $back_color, $font_color) {
		$this->title = $title;
		$this->position = $position;
		$this->background_color = $back_color;
		$this->font_color = $font_color;
	}

	// Show the header into a page
	public function show() {
		echo '<h1 ';
		echo 'style="text-align: ' . $this->position . '; ';
		echo 'background-color: ' . $this->background_color . '; ';
		echo 'color: ' . $this->font_color . '; ';
		echo '">' . $this->title . '</h1>';
	}
	
}
// Get the values past by the form
$title_options2 = $_POST['title'];
// Generates a new PageHeader object
$page_header2 = new PageHeader2($title_options[0],$title_options[1],$title_options[2],$title_options[3]);


// 12. 
/*
Class that creates a table
*/
class Tabla {
	
	private $mat=array();
	private $cantFilas;
	private $cantColumnas;
	private $background_color;	// The table background color
	private $font_color;		// The table font color
	public function __construct($fi,$co,$bc,$fc) {
    	$this->cantFilas = $fi;
		$this->cantColumnas = $co;
		$this->background_color = $bc;
		$this->font_color = $fc;
	}

	public function cargar($fila,$columna,$valor) {
		$this->mat[$fila][$columna]=$valor;
	}

	private function inicioTabla() {
		echo '<table border="1" style="background-color: ' . $this->background_color . '; color: ' . $this->font_color. '">';
	}

	private function inicioFila() {
		echo '<tr>';
	}

	private function mostrar($fi,$co) {
		echo '<td>'.$this->mat[$fi][$co].'</td>';
	}

	private function finFila() {
		echo '</tr>';
	}

	private function finTabla() {
		echo '</table>';
	}

	public function graficar() {
	//Hacer esta funcion
		$this->inicioTabla();
		for ($i = 0; $i < $this->cantFilas; $i++) {
			$this->inicioFila();
			for ($j = 0; $j < $this->cantColumnas; $j++) {
				$this->mostrar(($i + 1), ($j + 1));
			}
			$this->finFila();
		}
		$this->finTabla();
	}
}

$tabla1=new Tabla(2,3,'red','blue');
$tabla1->cargar(1,1,"1");
$tabla1->cargar(1,2,"2");
$tabla1->cargar(1,3,"3");
$tabla1->cargar(2,1,"4");
$tabla1->cargar(2,2,"5");
$tabla1->cargar(2,3,"6");

// 13. 
class Menu {
	private $enlaces=array();
	private $titulos=array();
	public function cargarOpcion($en,$tit) {
		$this->enlaces[]=$en;
		$this->titulos[]=$tit;
	}
	private function mostrarHorizontal() {
		//hacer esta funcion	
		for($f=0;$f<count($this->enlaces);$f++) {
    		echo '<a href="'.$this->enlaces[$f].'">'.$this->titulos[$f].'</a>';
    	}
	}
	private function mostrarVertical() {
		for($f=0;$f<count($this->enlaces);$f++) {
			echo '<a href="'.$this->enlaces[$f].'">'.$this->titulos[$f].'</a>';
			echo "<br>";
		}
	}

	public function mostrar($orientacion) {
		//hacer esta funcion
		if ($orientacion == "horizontal") {
			$this->mostrarHorizontal();
		} else {
			$this->mostrarVertical();
		}
	}
}
 

// 14.
/**
Class that generates a page header
*/
class PageHeader3 {
	// Properties
	private $title;				// The page header title
	private $position;			// The page header position (right,center,left) ?
	private $background_color;	// The header background color
	private $font_color;		// The page header font color

	// Default constructor
	public function __construct() {
		$this->title = "Title";
		$this->position = "center";
		$this->background_color = "black";
		$this->font_color = "purple";
	}
/*
	// Constructor
	public function __construct($title, $position, $back_color, $font_color) {
		$this->title = $title;
		$this->position = $position;
		$this->background_color = $back_color;
		$this->font_color = $font_color;
	}
*/
	// Show the header into a page
	public function show() {
		echo '<h1 ';
		echo 'style="text-align: ' . $this->position . '; ';
		echo 'background-color: ' . $this->background_color . '; ';
		echo 'color: ' . $this->font_color . '; ';
		echo '">' . $this->title . '</h1>';
	}
	
}
// Generates a new PageHeader object
$page_header3 = new PageHeader3();

?>



<!-- SHOW RESULTS -->


<h3>Part 1</h3>
<p>Result = <?php sumFiveNum($number_to_sum[0] ,$number_to_sum[1],$number_to_sum[2],$number_to_sum[3],$number_to_sum[4]); ?></p>
<hr>
<h3>Part 2</h3>
<p>Result = <?php echo $temp ; ?></p>
<hr>
<h3>Part 3</h3>
<p>Result = <?php echo $volume_cilinder ; ?></p>
<hr>
<h3>Part 4</h3>
<p>Price: <?php echo $purchase_price; ?></p>
<p>Price with discount: <?php echo $price_with_discount; ?></p>
<h3>Part 5</h3>
<p>Relation: <?php echo $comparation_result; ?></p>
<h3>Part 6</h3>
<p>Middle point: <?php echo $mid_point; ?></p>
<h3>Part 7</h3>
<p>Middle point: <?php print_r($separated); ?></p>
<h3>Part 8</h3>
<p>Employee 1 <?php $emp1->impuestos(); ?></p>
<p>Employee 2 <?php $emp2->impuestos(); ?></p>
<h3>Part 9</h3>
<?php $list1->mostrar(); ?>
<h3>Part 10</h3>
<?php $page_header->show(); ?>
<h3>Part 11</h3>
<?php $page_header2->show(); ?>
<h3>Part 12</h3>
<?php $tabla1->graficar(); ?>
<h3>Part 13</h3>
<?php
$menu1=new Menu();
$menu1->cargarOpcion('http://www.lanacion.com.ar','La Nación');
$menu1->cargarOpcion('http://www.clarin.com.ar','El Clarín');
$menu1->cargarOpcion('http://www.lavoz.com.ar','La Voz del Interior');
$menu1->mostrar("horizontal");
echo '<br>';
$menu2=new Menu();
$menu2->cargarOpcion('http://www.google.com','Google');
$menu2->cargarOpcion('http://www.yahoo.com','Yhahoo');
$menu2->cargarOpcion('http://www.msn.com','MSN');
$menu2->mostrar("vertical");
?>
<h3>Part 13</h3>
<?php $page_header3->show(); ?>

