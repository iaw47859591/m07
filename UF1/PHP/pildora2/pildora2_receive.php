<?php
// Declare and get the variables
$first_number = (int) $_POST["op1"];
$operator = $_POST["operator"];
$second_number = (int) $_POST["op2"];
$result = 0;
// Calculate the operation with the correspondent operator
switch ($operator) {
	case "+":
		$result = $first_number + $second_number;
		break;

	case "-":
		$result = $first_number - $second_number;
		break;
		
	case "*":
		$result = $first_number * $second_number;
		break;
		
	case "/":
		$result = number_format(($first_number / $second_number), 2);
		break;
		
	case "EXP":
		$result = $first_number ** $second_number;
		break;
	default: 
		$result = "ERR";
}

//Show the result
echo '<p>' . $first_number . ' ' . $operator . ' ' . $second_number . ' = ' . $result . '</p>';
?>
