<h1>Pildora 1<h1>
<table>
<tr>
<th>Operations</th>
<th>Value</th>
</tr>

<?php 
// Variables declaration
$a = 4;
$b = 3;	

// Operations
$sum = $a + $b;
$sub = $a + $b;
$mul = $a * $b;
$div = $a / $b;
$exp = $a ** $b;

// Print into the table
echo '<tr>';
echo '<td> ' . 'A' . '</td>';
echo '<td> ' . $a . '</td>';
echo '</tr>';
echo '<tr>';
echo '<td> ' . 'B' . '</td>';
echo '<td> ' . $b . '</td>';
echo '</tr>';
echo '<tr>';
echo '<td> ' . 'A+B' . '</td>';
echo '<td> ' . $sum . '</td>';
echo '</tr>';
echo '<tr>';
echo '<td> ' . 'A-B' . '</td>';
echo '<td> ' . $sub . '</td>';
echo '</tr>';
echo '<tr>';
echo '<td> ' . 'A*B' . '</td>';
echo '<td> ' . $mul . '</td>';
echo '</tr>';
echo '<tr>';
echo '<td> ' . 'A/B' . '</td>';
echo '<td> ' . number_format($div, 1) . '</td>';
echo '</tr>';
echo '<tr>';
echo '<td> ' . 'AexpB' . '</td>';
echo '<td> ' . $exp . '</td>';
echo '</tr>';

?>
</table>
