var mongoose = require("mongoose"),
Alumno = require("../models/alumnos");
// load from DB function
exports.load = async (req, res, next) => {
    try {
        const alumnos = await Alumno.find();
        if (req.isApi) {
            res.json(alumnos);
        } else {
            return alumnos;
        }
    } catch (error) {
      console.log(error);
    } 
}

// Cargar alumno
exports.loadOne = async (req, res, next) => {
    try {
        const alumno = await Alumno.findById(req.params.id);
        if(req.isApi) {
            res.json(alumno);
        } else {
            return alumno;
        }
    } catch (error) {
        console.log(error); 
    } 
}

// Añadir Alumno
exports.add = async (req, res, next) => {
    const alumno = new Alumno({
        nombre: req.body.nombre,
        apellido: req.body.apellido
    })
   try {
       const guardado = await alumno.save();
       if(req.isApi) {
           res.status(201).json(guardado);
       } else {
           return guardado;
       }
   } catch (error) {
       console.log(error);
   }
}

//Eliminar Alumno
exports.delete = async (req, res) => {
    try {
        const alumno = await Alumno.findById(req.params.id);
        const resultado = alumno.delete();
        if (req.isApi) {
            res.json({ mesagge: "Eliminado"});
        } else {
            return "Eliminado";
        }        
    } catch (error) {
        console.log(error); 
    } 
}

// Modificar Alumno
exports.update = async (req,res) => {
    const alumno = {
        nombre: req.body.nombre,
        apellido: req.body.apellido
    }
    try {
        const actu = await Alumno.updateOne({_id: req.params.id}, alumno)
        if(req.isApi) {
            res.status(200).json({ mesagge: "Actualizado"});
        } else {
            return "Actualizado";
        }
    } catch (error) {
    }
}
