var mongoose = require("mongoose"),
Asignatura = require("../models/asignatura");
// load from DB function
exports.save = async (req, res, next) => {
    const asignatura = new Asignatura({
        nombre: req.body.name,
        numHoras: req.body.numHoras,
        docente: req.body.docente,
        alumno: req.body.alumno
    })
    try {
        const guardado = await asignatura.save();
        if (req.isApi) {
            res.status(201).json(guardado);
        } else {
            return guardado;
        }
    } catch (error) {
        console.log(error);
    }
}

exports.load = async (req, res, next) => {
    try {
        const asignatura = await Asignatura.find();
        if (req.isApi) {
            res.json(asignatura);
        } else {
            return asignatura;
        }
    } catch (error) {
      console.log(error);
    } 
}

// Añadir Asignatura
exports.add = async (req, res, next) => {
    const asignatura = new Asignatura({
        nombre: req.body.name,
        numHoras: req.body.numHoras,
        docente: req.body.docente,
        alumno: req.body.alumno
    })
   try {
       const guardado = await asignatura.save();
       if(req.isApi) {
           res.status(201).json(guardado);
       } else {
           return guardado;
       }
   } catch (error) {
       console.log(error);
   }
}

//Eliminar Asignatura
exports.delete = async (req, res) => {
    try {
        const asignatura = await Asignatura.findById(req.params.id);
        const resultado = asignatura.delete();
        if (req.isApi) {
            res.json({ mesagge: "Eliminado"});
        } else {
            return "Eliminado";
        }        
    } catch (error) {
        console.log(error); 
    } 
}

// Modificar Alumno
exports.update = async (req,res) => {
    const asignatura = {
        nombre: req.body.name,
        numHoras: req.body.numHoras,
        docente: req.body.docente,
        alumno: req.body.alumno
    }
    try {
        const actu = await Asignatura.updateOne({_id: req.params.id}, asignatura)
        if(req.isApi) {
            res.status(200).json({ mesagge: "Actualizado"});
        } else {
            return "Actualizado";
        }
    } catch (error) {
    }
}
