var mongoose = require("mongoose"),
Docente = require("../models/docente");
// load from DB function
exports.load = async (req, res, next) => {
    try {
        const docente = await Docente.find();
        if (req.isApi) {
            res.json(docente);
        } else {
            return docente;
        }
    } catch (error) {
      console.log(error);
    } 
}

// Ver un Docente
exports.loadOne = async (req, res, next) => {
    try {
        console.log(req.params.id);
        const docente = await Docente.findById(req.params.id);
        if(req.isApi) {
            res.json(docente);
        } else {
            return docente;
        }
    } catch (error) {
        console.log(error); 
    } 
}

// Añadir Docente
exports.add = async (req, res, next) => {
    const docente = new Docente({
        nombre: req.body.nombre,
        apellido: req.body.apellido
    })
   try {
       const guardado = await docente.save();
       if(req.isApi) {
           res.status(201).json(guardado);
       } else {
           return guardado;
       }
   } catch (error) {
       console.log(error);
   }
}

//Eliminar Docente
exports.delete = async (req, res) => {
    try {
        const docente = await Docente.findById(req.params.id);
        const resultado = docente.delete();
        if (req.isApi) {
            res.json({ mesagge: "Eliminado"});
        } else {
            return "Eliminado";
        }        
    } catch (error) {
        console.log(error); 
    } 
}

// Modificar Docente
exports.update = async (req,res) => {
    const docente = {
        nombre: req.body.nombre,
        apellido: req.body.apellido
    }
    try {
        const actu = await Docente.updateOne({_id: req.params.id}, docente)
        if(req.isApi) {
            res.status(200).json({ mesagge: "Actualizado"});
        } else {
            return "Actualizado";
        }
    } catch (error) {
        
    }
}
