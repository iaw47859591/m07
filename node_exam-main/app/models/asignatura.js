var mongoose = require('mongoose');
Schema = mongoose.Schema;

var AsignaturaSchema = new mongoose.Schema({
    nombre: { type: String },
    numHoras: { type: String }, 
    docente: { type: String }, 
    alumno: { type: String }
});

module.exports = mongoose.model("Asignatura", AsignaturaSchema);
