var express = require("express");
var router = express.Router();
//Controladores
var path = require("path");
var ctrlDir = "/app/app/controllers";
var alumnrCtrl = require(path.join(ctrlDir, "alumnos"));
var docenteCtrl = require(path.join(ctrlDir, "docentes"));
var asignaturaCtrl = require(path.join(ctrlDir, "asignaturas"));

router.get("/asignatura/new", async function(req, res, next) {
    alumnos = await alumnrCtrl.load(req);
    docentes = await docenteCtrl.load(req);        
    res.render("newAsignatura",{listaAlumnos: alumnos, listaDocentes: docentes})
});

router.post("/asignatura/save", async function(req, res, next) {
    result = await asignaturaCtrl.save(req);
    if (result) {
        res.send("Añadido");
    } else {
        res.send("No añadido");
    }
});


router.get("/chat", function(req, res, next) {
    res.render("chat")
});

router.get("/chat/:id", function(req, res, next) {
    res.render("chat");
});

module.exports = router;

