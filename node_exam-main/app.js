require("dotenv").config();
const port = process.env.SERVER_PORT || 6000;
var express = require("express"),
  //bodyParser = require("body-parser"),
  app = express(),
  server = require("http").createServer(app),
  path = require("path"),
  io = require("socket.io")(server);
//MONGO
var mongoose = require('mongoose');
//BODY-PARSER
//app.use(bodyParser.urlencoded({ extended: true}));


server.listen(port, (err, res) => {
  if (err) console.log(`ERROR: Connecting APP ${err}`);
  else console.log(`Server is running on port ${port}`);
});
//Connect to mongodb://devroot:devroot@mongo:27017/chat?authSource=admin
mongoose.connect(
  `mongodb://${process.env.MONGO_ROOT_USER}:${process.env.MONGO_ROOT_PASSWORD}@${process.env.MONGO_URI}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}?authSource=admin`,
  { useCreateIndex: true, useUnifiedTopology: true, useNewUrlParser: true },
  (err, res) => {
    if (err) console.log(`ERROR: connecting to Database.  ${err}`);
    else console.log(`Database Online: ${process.env.MONGO_DB}`);
    console.log(`mongodb://${process.env.MONGO_ROOT_USER}:${process.env.MONGO_ROOT_PASSWORD}@${process.env.MONGO_URI}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}?authSource=admin`);
  }
);

// Import routes of our app

var routes = require("./app/routes/app");
var api = require("./app/routes/api");
var handlerError = require("./app/routes/handler");

// view engine setup and other configurations
app.set("views", path.join(__dirname, "app", "views"));
app.set("view engine", "pug");
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "public")));

// Define routes using URL path

app.use("/", routes);
//API
app.use('/api',(req, res, next) =>{
  req.isApi=true;
  next();
});
app.use("/api", api);

app.use(handlerError);

/*Socket functions */
io.on("connection", (socket) => {
  // console.log(socket);
  var room = "all";
  socket.join(room);
  // New msg
  socket.on("newMsg", (data) => {
    if (data.room != "all") {
      socket.broadcast.to(room).emit("newMsg", data);  
    } else {
      socket.broadcast.emit("newMsg", data);
    }
  })
  // Change room
  socket.on("changeRoom", (newRoom) => {
    socket.leave(room);
    socket.join(newRoom);
    room = newRoom;
    socket.emit("changeRoom", newRoom);
  })
})
module.exports = app;
