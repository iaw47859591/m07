$(document).ready(function () {
  //Inicializa socket con IO
  const socket = io();

  

  //Cuando cambia el select redirigimos a la URL del chat
  $('#selectRoom').on("change",()=>{
    var sala = $(this).find("option:selected").val();
    socket.emit("changeRoom", $('#selectRoom').val());
    // window.location.href = "/chat/"+sala;
    window.history.pushState(null,"Chat","/chat/"+sala);
    $(this).val(sala);
  })

  
  //Accion cuando el usuario envia mensaje con submit
  $("#chat").submit(function (e) {
    e.preventDefault();
    var msg = $("#msg").val();
    var autor = $("#autor").val();
    var room = $('#selectRoom').val();
    $("#chatBox").append(`<p>${msg}<p>`);

    var toSend = {user:autor, text:msg, room: room};
    socket.emit("newMsg", toSend);
  });

  //Acciones a realizar cuando se detecta actividad en el canal newMsg
  socket.on("newMsg", (data)=> {
    $("#chatBox").append(`<p>${data.text}<p>`);
  })

  socket.on("changeRoom", (room) => {
    $("#chatBox").html('').append(`<p>Sala ${room}<p>`)
  })

});
