<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use App\Exceptions\QueryException;
use App\Exceptions\FileException;
use App\Exceptions\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        if ($exception instanceof QueryException) {
            $error = "ERROR: No DDBB acces";
            return redirect(url()->previous())->withError($error);
        }
        if ($exception instanceof FileException) {
            $error = "ERROR: There is no permissions for uploading the image";
            return redirect(url()->previous())->withError($error);
        }

        if ($exception instanceof NotFoundHttpException) {
            return response()->view('pageNotFound');
        }
        return parent::render($request, $exception);
    }
}
