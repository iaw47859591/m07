<?php

namespace App\Http\Controllers;

use App\Models\Product;
use GuzzleHttp\Psr7\UploadedFile;
use Illuminate\Http\Request;
//use Illuminate\Http\UploadedFile;

class CompraController extends Controller
{

    public function main(Request $request)
    {
        /*Recuerda el estado de la compra y redirige a la pantalla en la que el usuario estaba antes: resumen, envio o confirmar */
        $state = $request->session()->get('state'); 
        if ($state == '') {
            return redirect('compra/resumen');
        }
        return redirect('/compra/' . $state);
    }

    public function carrito(Request $request) {
        
        // Producto
        $prod_id = $request->get('prod_id');
        $qty = $request->get('qty');

        $productName = Product::query('products')->where('id', $prod_id)->value('name');
        $productImage  = Product::query('products')->where('id', $prod_id)->value('image');
        $productDesc = Product::query('products')->where('id', $prod_id)->value('description');
        $productPrice = Product::query('products')->where('id', $prod_id)->value('price');
      
      
//      $producto = Product::query('products')->where('id', $prod_id)->get();

            $producto = [
            'id' => $prod_id,
            'name' => $productName,
            'image' => $productImage,
            'description' => $productDesc,
            'price' => $productPrice,
        ];

        for ($i = 1; $i < $qty; $i++) {
        $carrito = $request->session()->get('carrito', []);
        array_push($carrito, (object) $producto);           
        $request->session()->put('carrito', $carrito);
        }

        $carrito = $request->session()->get('carrito', []);
        array_push($carrito, (object) $producto);           
        $request->session()->put('carrito', $carrito);

        $request->session()->put('state', '');
//        $request->session()->flush('carrito');
//        $ carrito = [];
        return [$carrito, $qty];
    }


    public function remove(Request $request) {
        $prod_id = $request->get('prod_id'); 
        $found = false;
        $carrito = $request->session()->get('carrito', []);

        for ($i = 0; $i < count($carrito); $i++) {
            if ($carrito[$i]->id == $prod_id && $found) {
                unset($carrito[$i]);
                $found = true;
            }
        }
        $request->session()->put('carrito', $carrito);
        
        $products = [];
        foreach($carrito as $prod) {
            array_push($products, (object) $prod);
            
        }
        return view('compra/resumen')
            ->with('products', $products);
    }
    /**
     * Method to show the resume of the products in the chart
     */
    public function resumen(Request $request)
    {
        $request->session()->put('state', 'resumen');
        //Dummy: hay que cambiar la info por la información guardada en el carrito (session)
        $carrito = session()->get('carrito');
        $products = [];
        foreach($carrito as $prod) {
            array_push($products, (object) $prod);
            
        }
//        $products = [
//            (object) [
//                'name' => 'Lego 1', 'category' => 0, 'description' => 'Esto es la descripcion lego 1', 'price' => 10.02, 'image' => 'lego1.jpeg', 'rating' => 2
//            ],
//            (object) [
//                'name' => 'Lego 1', 'category' => 0, 'description' => 'Esto es la descripcion lego 1', 'price' => 10.02, 'image' => 'lego1.jpeg', 'rating' => 2
//            ],
//        ];


        return view('compra/resumen')
            ->with('products', $products);

    }

    /**
     * Method to show and process the shipping form (envio)
     */
    public function envio(Request $request)
    {
        $request->session()->put('state', 'envio');
        return view('compra/envio');
    }
    /**
     * Method to show and process the shipping form (envio)
     */
    public function verificarEnvio(Request $request)
    {
        $formOK = false;

        /* PONER AQUI TODO LO NECESARIO PARA VERIFICAR EL FORMULARIO */
         $this->validate($request, [
            "name" => 'required',
            "email" => 'required',
            "adress" => 'required',
            "password" => 'required',
            "password-confirm" => 'required',
            "foto" => 'required'
        ]);

//        $formOK = $request->validate([
//            "name" => ["required"],
//            "email" => ["required"],
//            "adress" => ["required"],
//            "password" => ["required"],
//            "pasword-confirm" => ["required"],
//            "foto" => ["required"],
//        ]);

        /*Una vez verificado se guarda la información de envio en la session*/
        if ($request) {
            $formOK = true;
            $shipping = $request->session()->get('shipping', []);

//            $file = $request->file('foto');
//            $destinationPath = public_path().'/img/users';
//            $originalFile = $file->getClientOriginalName();
//            var_dump("file". $file);
//            $file->move($destinationPath, time().$originalFile);

            $file = $request->file('foto');
            $destinationPath = public_path().'/img/users';
            $originalFile = $file->getClientOriginalName();
            $file->move($destinationPath, $originalFile);
            
            $client = [
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'adress' => $request->input('adress'),
                'image' => $file->getClientOriginalName()
            ];
            
            $request->session()->put('shipping', $client);
//            $request->session()->flush();
        }

        //si el formulario se ha rellenado correctamente se redirecciona a la pagina de confirmación
        if ($formOK) {
            return redirect('/compra/confirmar');
        }
        return view('compra/envio');
    }
    /**
     * Method to show the list of procuts and shipping info
     */
    public function confirmar(Request $request)
    {
        $request->session()->put('state', 'confirmar');
        //Dummy: hay que cambiar la info por la información guardada en session
        $products = session()->get('carrito');
        $shipping = session()->get('shipping');
        $image = $shipping['image'];
        
//        $products = [];
//        $shipping = [];
//            foreach ($carrito as $prod) {
//                array_push($products, $prod);
//            };
//            foreach ($dataShipping as $data) {
//                array_push($shipping, $data);
//            };
            
        return view('compra/confirmar')
            ->with('products', $products)
            ->with('shipping', $shipping)->with('image', $image);
    }

    public function end(Request $request) {
        $request->session()->flush();
        return view('compra/end');
    }
}
