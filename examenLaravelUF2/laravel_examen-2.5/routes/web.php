<?php

use App\Http\Controllers\CompraController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'ProductController@all');
Route::get('/category/{id?}', 'ProductController@category');
Route::post('/rating', 'ProductController@rating');

Route::get('/compra', 'CompraController@main');
Route::get('/compra/resumen', 'CompraController@resumen');
Route::get('/compra/envio', 'CompraController@envio');
Route::post('/compra/envio', 'CompraController@verificarEnvio');
Route::get('/compra/confirmar', 'CompraController@confirmar');

Route::post('/compra/remove', 'CompraController@remove');
Route::get('/compra/end', 'CompraController@end');
Route::get('/compra/removeAll', 'ProductController@removeAll');

Route::post('/stock', [ProductController::class, 'stock'])->name('stock');
Route::post('/carrito', [CompraController::class, 'carrito'])->name('carrito');
