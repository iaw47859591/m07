@extends('layouts.app')

@section('content')
-
@php
$total=0
@endphp
@foreach ($products as $product)
@php
$total+=$product->price
@endphp
@endforeach

<div class="row">

  <div class="col-lg-3">

    <h1 class="my-4">Lego Shop</h1>
    <ul class="list-group">
      <li class="list-group-item">
      Total de productos: {{sizeof($products)}}</li>
      <li class="list-group-item">
      Precio total de productos: {{$total}}</li>
    </ul>

  

  </div>
  <!-- /.col-lg-3 -->

  <div class="col-lg-9">
    <div class="row">
      @forelse ($products as $product)
      <div class="col-lg-4 col-md-6 mb-4">
        <div class="card h-100">
          <a href="#"><img class="card-img-top" src="{{ asset('img/'.$product->image) }}" alt=""></a>
          <div class="card-body">
            <h4 class="card-title">
              <a href="#">{{$product->name}}</a>
            </h4>
            <h5>${{$product->price}}</h5>
          </div>
        </div>
      </div>
      @empty
      <p>No products to show</p>
      @endforelse
    </div>
    <!-- /.row -->
    <a href="removeAll" class="btn btn-danger btn-lg pull-right">Remove ALL</a>
  </div>
  <!-- /.col-lg-9 -->
</div>


<a href="{{ url('/') }}" class="btn btn-secondary btn-lg float-left">Atras</a>
<a href="{{ url('/compra/envio') }}"  class="btn btn-primary btn-lg float-right">Siguiente</a>
<br><br>
<script>
$('.removeFromCart').submit(function(e){
    e.preventDefault();
    var data = $(this).serialize()
    
        console.log(data)
        axios({
        method: 'POST',
            url: 'remove', 
            responseType: 'json',
            data: data,
    })
        .then(response => {
        console.log(response);
    })
        .catch(e => {
        console.log(e);
        });
});
</script>

@endsection
