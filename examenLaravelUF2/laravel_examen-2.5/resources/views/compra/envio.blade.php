@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
  <div class="col-md-8">
    <div class="card">
      <div class="card-header">{{ __('Register') }}</div>

      <div class="card-body">
        <form method="POST" action="{{ url('/compra/envio') }}" enctype="multipart/form-data">
          @csrf

          <div class="form-group row">
            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

            <div class="col-md-6">
                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" autocomplete="name" autofocus>
              <!--Error name-->
                @error('name')
              <span class="invalid-feedback" role="alert">
                <strong>El nombre es obligatorio</strong>
              </span>
              @enderror
            </div>
          </div>
 <div class="form-group row">
            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

            <div class="col-md-6">
              <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email">
              <!--Error email-->
              @error('email')
              <span class="invalid-feedback" role="alert">
                <strong>El email es obligatorio</strong>
              </span>
              @enderror
            </div>
          </div>

          <div class="form-group row">
            <label for="adress" class="col-md-4 col-form-label text-md-right">{{ __('Direccion') }}</label>

            <div class="col-md-6">
              <input id="adress" type="text" class="form-control @error('adress') is-invalid @enderror" name="adress" value="{{ old('adress') }}" autocomplete="adress">
              <!--Error direccion-->
              @error('adress')
              <span class="invalid-feedback" role="alert">
                <strong>La direccion es obligatoria</strong>
              </span>
              @enderror
            </div>
          </div>

          <div class="form-group row">
            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

            <div class="col-md-6">
              <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" autocomplete="password">
              <!--Error password-->
              @error('password')
              <span class="invalid-feedback" role="alert">
                <strong>El password es obligatorio</strong>
              </span>
              @enderror
            </div>
          </div>

          <div class="form-group row">
            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

            <div class="col-md-6">
              <input id="password-confirm" type="password" class="form-control @error('password-confirm') is-invalid @enderror" name="password-confirm" value="{{ old('password-confirm') }}" autocomplete="password-confirm">
              <!--Error password confirm-->
              @error('password-confirm')
              <span class="invalid-feedback" role="alert">
                <strong>La verificación de password es obligatoria</strong>
              </span>
              @enderror
            </div>
          </div>

          <div class="form-group row">
            <label for="foto" class="col-md-4 col-form-label text-md-right">{{ __('Foto') }}</label>

            <div class="col-md-6">
              <input id="foto" type="file" class="form-control @error('foto') is-invalid @enderror" name="foto" value="{{ old('foto') }}">
              <!--Error foto-->
              @error('foto')
              <span class="invalid-feedback" role="alert">
                <strong>La foto es obligatoria</strong>
              </span>
              @enderror
            </div>
          </div>
          <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
              <button type="submit" class="btn btn-primary">
                {{ __('Pagar') }}
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>




<a href="{{ url('/compra/resumen') }}" class="btn btn-secondary btn-lg float-left">Atras</a>
<a href="{{ url('/compra/confirmar') }}"  class="btn btn-primary btn-lg float-right">Siguiente</a>
<!--<a href="{{ url('/compra/confirmar') }}"  class="btn btn-primary btn-lg float-right">Siguiente</a>-->

<br><br>
@endsection
