var mongoose = require('mongoose');
Schema = mongoose.Schema;

var ClientSchema = new mongoose.Schema({
    id: {type: String},
    name: {type: String},
    surname: {type: String },
})

module.exports = mongoose.model("Client", ClientSchema); 