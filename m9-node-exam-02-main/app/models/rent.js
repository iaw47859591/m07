var mongoose = require('mongoose');
Schema = mongoose.Schema;

var RentSchema = new mongoose.Schema({
    car: {type: String},
    client: {type: String },
    days: {type: String},
    price: {type: String}
})

module.exports = mongoose.model("Rent", RentSchema); 