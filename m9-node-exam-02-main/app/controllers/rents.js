var mongoose = require("mongoose"),
  Model = require("../models/rent"),
  Cars = require("../models/car");

exports.getAll = async (req, res, next) => {
  res.render("form")
};

exports.find = async (req, res, next) => {

};

exports.delete = async (req, res, next) => {

};

exports.add = async (req, res, next) => {
  //price: Precio del coche x el numero de dias. PAra saber el precio del coche debes obtener ANTES la info del coche seleccionado
  const carToFind = await Cars.find({id: req.body.car});
  const carPrice = parseFloat(carToFind[0].price) 
  const totalPrice = carPrice * req.body.rent;
  var el = new Model({
    car: req.body.car + "",
    client: req.body.client + "",
    days: req.body.rent + "",
    price: totalPrice + ""
  });
  console.log(el)
  //Operaciones para guardar en la DB
  try {
    const saved = await el.save();
    if (req.isApi) {
      res.status(201).json(saved);
    } else {
      return saved;
    }
  } catch (error) {
    console.log(error);
  }
};