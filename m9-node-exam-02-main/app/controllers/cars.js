var mongoose = require("mongoose"),
  Model = require("../models/car");

exports.getAll = async (req, res, next) => {
  try {
    const cars = await Model.find();
    if (req.isApi) {
      res.json(cars);
    } else {
      return cars;
    }
  } catch (error) {
    console.log(error);
  }
};

exports.find = async (req, res, next) => {

};
exports.create = async (req, res, next) => {
 
};
exports.delete = async (req, res, next) => {
 
};
