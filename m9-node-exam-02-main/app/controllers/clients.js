var mongoose = require("mongoose"),
  Model = require("../models/client");

exports.getAll = async (req, res, next) => {
  try {
    const clients = await Model.find();
    if (req.isApi) {
      res.json(clients);
    } else {
      return clients;
    }
  } catch (error) {
    console.log(error);
  }
};

exports.find = async (req, res, next) => {

};

exports.delete = async (req, res, next) => {

};
