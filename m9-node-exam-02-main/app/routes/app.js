var express = require("express"),
  path = require("path"),
  router = express.Router(),
  ctrlDir = "/app/app/controllers",
  carsCtrl = require(path.join(ctrlDir, "cars")),
  rentsCtrl = require(path.join(ctrlDir, "rents")),
  clientsCtrl = require(path.join(ctrlDir, "clients"));


//router.route("/chat/:id");
router.get("/rent/new", async (req, res) => {
  clients = await clientsCtrl.getAll(req);
  cars = await carsCtrl.getAll(req);          
  res.render("form",{clientList: clients, carsList: cars})
});
//router.route("/rent/list");
router.post("/rent/new", rentsCtrl.add);

module.exports = router;



                                                                   
 
